FROM chainguard/bash:latest

# thank you git for stealing my permission bits smh
COPY root_folder /root_folder
RUN bash -c "chmod 777 /root_folder/child_folder/file_with_weird_permissions"
RUN bash -c "chmod g+s /root_folder/child_folder/file_with_weird_permissions"

FROM scratch

COPY --from=0  /root_folder /root_folder
COPY fine-ill-give-you-a-command /
WORKDIR /
CMD ["/fine-ill-give-you-a-command"]